<p align="center"><a href="https://gitlab.com/contractuo_public/unit-test-assignment" target="_blank"><img src="https://www.contractuo.com/wp-content/uploads/2019/09/Logo-no-tagline-1.png" width="400"></a></p>

# Unit-test Assignment

We’ve setup this assignment with the following goals:
* Getting to know each other a little better
* Getting to know your strengths
* Getting to know your preferences

By conducting an assignment which is bigger than one can handle in the given time, you are forced to make choices: what do you find most important; what are your ‘go-to’ tasks, etc. So, don’t worry not finishing any of the work; that’s not the goal. Do remember to look around you and ask question; grasp the opportunity to get to know us!
You will get approximately 2 hours to work on the assignment. During that time: shout if you have any questions (or a need for coffee/break). Work it out in any way that you feel is appropriate. When time is up, we ask you to present your work shortly to some of us and have a short discussion.
During the assignment we will ask you to share your screen, this allows us to get a better understanding of your approach.
## The assignment

### Prerequisites

Below you’ll find some directions to setup your environment and the project.
1.  Optional: Setup your development environment; IDE, PHP7+ with composer and Node.JS
2.	Checkout the current repository
3.  Optional: install the dependencies with the following commands: composer install and npm install
4.  Use your favorite IDE/Text editor to browse trough the project files. 

### Project details

The assignment entails a Laravel project that contains logic for:
*	Blade and VueJS views for creating and editing a contract
*	Backend logic for creating and editing contract
*	User Register, login, logout logic
*	SQLite database file
*   Translations / migrations and models.

### Assignment details / requirements

The project is basically an implementation for a platform, where users can create and manage contracts that does not necessarily contain best practice code standards.

If you think it helps to setup the project and have it run on your local machine you can do so and ask our help where necessary
We would like you to get familiar with the project logic and come up with a theoretical approach to test it. 
We think that 2 hours is not enough time to actually write unit-tests and do not want to focus necessarily on your Laravel version X knowledge but more on the approach. 

Deliverables:

* Summarization of logic in the platform that has/can be tested.
* Your approach in testing the logic, with any tools you prefer to write and create pseudocode/illustrations or perhaps even PHP-Laravel code.
* Your feedback about the logic written in the ContractController:
  Does is contain best practice code standards? 
  Is request data validated properly? 
  ..?
  

If you think it makes sense to change deliverables or have any questions at all, don't hesitate to let us know ;)



