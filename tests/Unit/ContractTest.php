<?php

namespace Tests\Unit;

use App\Models\Contract;
use Database\Factories\ContractFactory;
use Faker\Factory;
use Tests\TestCase;

class ContractTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_example() {
        $contract = Contract::factory()->create();
        $this->assertTrue($contract->title === 'testtitle');
    }
}
