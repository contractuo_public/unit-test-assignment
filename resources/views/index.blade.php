@extends('layouts.app')

@section('content')
    <div id="app">
        <index
            name="{{Auth::user()->name}}"
            :contracts="{{$contracts}}"
        ></index>
    </div>
@endsection
