<?php

return [
    'save' => 'Save',
    'title' => 'Contract Title',
    'delete' => 'Delete Contract',
    'create' => 'Create Contract',
    'contract_created_succesfully' => 'Contract Created Successfully',
    'errors' => [
        'create_contract' => 'Something went wrong creating the contract, please contact the helpdesk or fix it ;)',
        'retrieve_contract' => 'Something went wrong with retrieving the contract, please contact the helpdesk or fix it ;)'
    ],
];
